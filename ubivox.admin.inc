<?php

/**
 * @file
 * Admin page callbacks for the ubivox module.
 */

/**
 * Ubivox administration form callback.
 * @return array
 *   Renderable array with the module's administration form.
 */
function ubivox_admin_form() {

  // Settings for anonymous users.
  $form['ubivox'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ubivox Account'),
  );
  $form['ubivox']['ubivox_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('ubivox_username'),
    '#description' => t('Ubivox username to connect to Ubivox API.'),
    '#required' => TRUE,
  );

  $form['ubivox']['ubivox_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('ubivox_password'),
    '#description' => t('Ubivox password to connect to Ubivox API.'),
    '#required' => TRUE,
  );

  $form['ubivox']['ubivox_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#default_value' => variable_get('ubivox_url'),
    '#description' => t('Ubivox account URL.'),
    '#required' => TRUE,
  );

  $form['ubivox_maillists'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ubivox mailing lists'),
  );

  $form['ubivox_maillists']['refresh_describtion'] = array(
    '#type' => 'markup',
    '#markup' => '<div>' . t('Click to get the latest mailing lists from Ubivox service.') . '</div>',
  );
  $form['ubivox_maillists']['refresh_lists'] = array(
    '#type' => 'submit',
    '#value' => t('Refresh mailing lists'),
    '#submit' => array('ubivox_fetch_mallist_data'),
  );

  $lists = _ubivox_get_maillists();
  if (!empty($lists)) {
    $markup = '<h4>Lists</h4><ul><li>' . implode('</li><li>', $lists) . "</li></ul>";
    $form['ubivox_maillists']['lists'] = array(
      '#type' => 'markup',
      '#markup' => $markup,
    );
  }
  $form['#submit'][] = 'ubivox_admin_form_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for the Ubivox subscribtion form.
 *
 * Connects to Ubivox API to get the mailing lists for the specified account.
 */
function ubivox_admin_form_submit($form, &$form_state) {

  $ubivox = ubivox_get_api_object(
    $form_state['values']['ubivox_username'],
    $form_state['values']['ubivox_password'],
    $form_state['values']['ubivox_url']);

  try {
    $maillists = $ubivox->call('ubivox.list_maillists', array());
  }
  catch (UbivoxAPIUnauthorized $e) {
    drupal_set_message(t('Unable to authorize you to Ubivox. Check your username and password.'), 'error');
  }
  catch (UbivoxAPIUnavailable $e) {
    drupal_set_message(t('Ubivox API is unavailable at the moment.'), 'warning');
  }
  catch (UbivoxAPINotFound $e) {
    drupal_set_message(t('Ubivox API was not found.'), 'warning');
  }
  variable_set('ubivox_maillists', $maillists);

}
