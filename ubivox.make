api = 2
core = 7.x

; Libraries
libraries[ubivox-php-api][download][type] = "file"
libraries[ubivox-php-api][download][url] = "https://bitbucket.org/ubivox/ubivox-api-php/get/0cc258db5ccc.zip"
libraries[ubivox-php-api][directory_name] = "ubivox-php-api"
libraries[ubivox-php-api][destination] = "libraries"

