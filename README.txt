The module provides integration with the Ubivox email delivery service.

## Description
After Ubivox credentials added the module will provide a subscribe form block
and panel pane for subscribing to one or more ubivox mailing lists. The module
also provides rss feeds at /ubivox_feed.xml to import into Ubivox system in order
to add site content inside newsletters.

## Installation
  * The module relies on the Ubivox-php-api library and it should exist in libraries folder.
  Current version of module is using version 0.1 of the library that could be
  downloaded from (https://bitbucket.org/ubivox/ubivox-api-php/get/0cc258db5ccc.zip)
    Libraries directory structure should be as follows:

    - libraries/
      - ubivox-php-api/
        - IXR_Library.php
        - README.php
        - ubivox_api.php

  If you are using drush make to build your project there is a makefile included as well.

  * The right Ubivox credentials (username, password, url) should be provided
   at admin/config/system/ubivox.
  * You need to have at least one list created in Ubivox and set
  as active in admin/config/system/ubivox in order users to be able to subscribe.

## Feeds
The module defines Ubivox feed view that can be accessed at /ubivox_feed.xml
The results of the view could be filtered as follows:

  - /ubivox_feed.xml/[content_type]
  - /ubivox_feed.xml/all/[term_id]
  - /ubivox_feed.xml/[content_type]/[term_id]
