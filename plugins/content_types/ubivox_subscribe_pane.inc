<?php
/**
 * @file
 * Defines Ubivox subscribtion panel pane.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Ubivox subscribtion'),
  'description' => t('Shows Ubivox maillists subscribe form'),
  'category' => t('Newsletters'),
  'edit form' => 'ubivox_subscribe_pane_edit_form',
  'render callback' => 'ubivox_subscribe_pane_render',
  'defaults' => array('active_maillists' => 0),
  'all contexts' => TRUE,
);

/**
 * An edit form for the pane's settings.
 */
function ubivox_subscribe_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['active_maillists'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Ubivox active maillists'),
    '#options' => _ubivox_get_maillists(),
    '#default_value' => array_filter(array_values($conf['active_maillists'])),
    '#description' => t('Select which maillists users can subscribe to.'),
  );
  return $form;
}
/**
 * Validate handler for ubivox_subscribe_pane.
 *
 * Validates if at least one mailing list is selected when adding the pane.
 */
function ubivox_subscribe_pane_edit_form_validate($form, &$form_state) {
  $selected_lists = array_filter($form_state['values']['active_maillists']);
  if (empty($selected_lists)) {
    form_set_error('active_maillists', t('You must set at least one mailing list as active.'));
  }
}

/**
 * Submit handler for ubivox_subscribe_pane.
 */
function ubivox_subscribe_pane_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the pane.
 */
function ubivox_subscribe_pane_render($subtype, $conf, $args, $contexts) {
  $active_list_ids = array_filter(array_values($conf['active_maillists']));
  $all_lists = _ubivox_get_maillists();
  $options = array();
  foreach ($active_list_ids as $active_list_id) {
    if (isset($all_lists[$active_list_id])) {
      $options[$active_list_id] = $all_lists[$active_list_id];
    }
  }
  $form = drupal_get_form('ubivox_subscribe_form', array('maillists' => $options));

  $block = new stdClass();
  $block->title = t('Newsletter subscribtion');
  $block->content = $form;

  return $block;
}
