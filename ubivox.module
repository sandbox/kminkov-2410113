<?php

/**
 * @file
 * Ubivox integration
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function ubivox_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && $plugin_type == 'content_types') {
    return 'plugins/' . $plugin_type;
  }
}

/**
 * Implements hook_menu().
 *
 * Adds custom menu callbacks.
 */
function ubivox_menu() {
  $items = array();

  $items['admin/config/system/ubivox'] = array(
    'title' => 'Ubivox',
    'description' => 'Configure settings for your Ubivox account',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ubivox_admin_form'),
    'access arguments' => array('administer ubivox settings'),
    'file' => 'ubivox.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Get Ubivox API object for connecting to Ubivox service.
 * @return UbivoxAPI
 *   Instance of UbivoxAPI object
 */
function ubivox_get_api_object($user = '', $password = '', $url = '') {
  $ubivox = &drupal_static(__FUNCTION__);
  if (isset($ubivox)) {
    return $ubivox;
  }

  if (($library = libraries_load('ubivox-php-api')) && empty($library['loaded'])) {
    $msg = t('Ubivox-php-api library not found.');
    watchdog('Ubivox', $msg, array(), WATCHDOG_ERROR);
    drupal_set_message($msg, 'error');
    return NULL;
  }
  // If credentials are not specified on function call
  // get them form the configs.
  if (empty($user) && empty($pass) && empty($url)) {
    $user = variable_get('ubivox_username');
    $password = variable_get('ubivox_password');
    $url = variable_get('ubivox_url');
  }

  if (empty($user) || empty($password) || empty($url)) {
    watchdog('ubivox', 'Some or all of Ubivox account credentials are missing.', NULL, WATCHDOG_ERROR);
    return NULL;
  }

  $ubivox = new UbivoxAPI($user, $password, $url);
  return $ubivox;
}

/**
 * Implements hook_libraries_info().
 */
function ubivox_libraries_info() {
  $libraries['ubivox-php-api'] = array(
    'name' => 'Ubivox PHP API',
    'version' => '0.1',
    'vendor url' => 'https://ubivox.com',
    'download url' => 'https://bitbucket.org/ubivox/ubivox-api-php/get/0cc258db5ccc.zip',
    'version arguments' => array(
      'file' => 'ubivox_api.php',
      'pattern' => '/\"UBIVOX_API_CLIENT_VERSION", \"((\d+)\.(\d+))\"/',
    ),
    'files' => array(
      'php' => array('ubivox_api.php'),
    ),
  );

  return $libraries;
}

/**
 * Helper function to get a list of all mailing lists.
 *
 * @return array
 *   Array of mailing lists.
 */
function _ubivox_get_maillists() {
  $maillists_formatted = &drupal_static('ubivox_maillists');
  if (isset($maillists_formatted)) {
    return $maillists_formatted;
  }

  $maillists = variable_get('ubivox_maillists');
  if (empty($maillists)) {
    return array();
  }

  $maillists_formatted = array();
  foreach ($maillists as $maillist) {
    $maillists_formatted[$maillist['id']] = $maillist['title'];
  }

  return $maillists_formatted;
}

/**
 * Implements hook_views_api().
 */
function ubivox_views_api() {
  return array('api' => 2.0);
}

/**
 * Implements hook_views_default_views().
 */
function ubivox_views_default_views() {
  $module = 'ubivox';
  $directory = 'views';
  $extension = 'view.inc';
  $name = 'view';

  $return = array();
  // Find all the files in the directory with the correct extension.
  $files = file_scan_directory(drupal_get_path('module', $module) . "/$directory", "/.$extension/");
  foreach ($files as $path => $file) {
    require $path;
    if (isset($$name)) {
      $return[$$name->name] = $$name;
    }
  }

  return $return;
}

/**
 * Feed import managed by Elysia.
 *
 * Define cron job for importing Ubivox mailing list.
 */
function ubivox_cronapi($op, $job = NULL) {
  $items['fetch_ubivox_data_cron'] = array(
    'description' => 'Fetch ubivox mailing list data',
    'rule' => '*/15 * * * *',
    'callback' => 'ubivox_fetch_mailist_data',
  );
  return $items;
}

/**
 * Callback for cron fetch_ubivox_data_cron.
 *
 * Gets all available mailing lists for the Ubivox account.
 */
function ubivox_fetch_mailist_data() {
  $ubivox = ubivox_get_api_object();
  if (is_null($ubivox)) {
    return;
  }
  try {
    $maillists = $ubivox->call('ubivox.list_maillists', array());
  }
  catch (Exception $e) {
    watchdog('ubivox', 'Error occured while trying to fetch Ubivox mailing lists:' . $e->getMessage(), array(), WATCHDOG_ERROR);
  }
  drupal_set_message(t('Ubivox mailing lists are refreshed.'), 'status', FALSE);
  variable_set('ubivox_maillists', $maillists);

}

/**
 * Definition of Ubivox subscribtion form.
 */
function ubivox_subscribe_form($form, &$form_state) {
  $maillists = _ubivox_get_maillists();

  // If there are no ubivox maillists for this account.
  if (empty($maillists)) {
    return $form;
  }

  if (isset($form_state['build_info']['args']['0']['maillists'])
      && !empty($form_state['build_info']['args']['0']['maillists'])) {

    $active_lists = $form_state['build_info']['args']['0']['maillists'];
  }
  else {
    return $form;
  }

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address:'),
  );

  $form['ubivox_maillists'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Mailing lists:'),
    '#options' => $active_lists,
  );

  // If only one maillist is available do not display checkboxes.
  if (count($active_lists) == 1) {
    $form['ubivox_maillists']['#type'] = "hidden";
    $form['ubivox_maillists']['#value'] = current(array_keys($active_lists));
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );
  $form['#validate'][] = 'ubivox_subscribe_form_validate';
  return $form;
}

/**
 * Validate handler for the Ubivox subscribtion form.
 */
function ubivox_subscribe_form_validate($form, &$form_state) {
  if (empty($form_state['values']['email'])) {
    form_set_error('email', t('Please provide e-mail address.'));
  }
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('The email address appears to be invalid.'));
  }
  if (is_array($form_state['values']['ubivox_maillists']) && !array_filter($form_state['values']['ubivox_maillists'])) {
    form_set_error('email', t('You should select at least one newsletter to subscribe to.'));
  }
}

/**
 * Submit handler for the Ubivox subscribtion form.
 */
function ubivox_subscribe_form_submit($form, &$form_state) {

  $maillists = $form_state['values']['ubivox_maillists'];
  // If only one mailing list is set as active and so
  // the mailing lists field is hidden and value is only list_id.
  if (!is_array($maillists)) {
    $maillists = array($maillists);
  }
  else {
    $maillists = array_values(array_filter($form_state['values']['ubivox_maillists']));
  }

  $ubivox = ubivox_get_api_object();
  try {
    $params = array(
      $form_state['values']['email'],
      $maillists,
      TRUE,
    );

    $result = $ubivox->call('ubivox.create_subscription', $params);
  }
  catch (Exception $e) {
    /*Raises:
    1001: Invalid e-mail address
    1003: The user is already subscribed
    2001: Invalid mailing list
    2004: Opt-in not configured (on one or more lists).*/
    switch ($e->getCode()) {
      case 1001:
        drupal_set_message(t('The email address appears to be invalid.'), 'error');
        break;

      case 1003:
        drupal_set_message(t('Your e-mail address has already been subscribed.'), 'warning');
        break;

      case 2001:
        drupal_set_message(t('The mailing list you choose does not exists anymore.'), 'error');
        break;

      default:
        drupal_set_message(t('There was problem with your subscribtion. Please try again.'), 'error');
        watchdog('ubivox', 'An error occured while trying to subscribe %email: "%message"', array(
        '%message' => $e->getMessage(),
        '%email' => $form_state['values']['email']), WATCHDOG_ERROR);
        break;
    }
  }
  if (isset($result) && $result) {
    $all_maillists = _ubivox_get_maillists();
    $list_names = array();
    foreach ($maillists as $maillist_id) {
      $list_names[] = $all_maillists[$maillist_id];
    }
    drupal_set_message(t('You subscribed successfully to') . " " . implode(', ', $list_names), 'status');
  }
}

/**
 * Implements hook_block_info()
 */
function ubivox_block_info() {
  $blocks['ubivox_subscribe_form'] = array(
    'info' => t('Ubivox subscribe form'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function ubivox_block_configure($delta = '') {

  $form = array();
  if ($delta == 'ubivox_subscribe_form') {
    $form['active_maillists'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Ubivox active maillists'),
      '#options' => _ubivox_get_maillists(),
      '#default_value' => variable_get('ubivox_active_maillists'),
      '#description' => t('Select which maillists users can subscribe to.'),
    );
  }
  return $form;
}

/**
 * Implementation of hook_block_save().
 */
function ubivox_block_save($delta = '', $edit = array()) {
  if ($delta == 'ubivox_subscribe_form') {
    variable_set('ubivox_active_maillists', array_filter(array_values($edit['active_maillists'])));
  }
}

/**
 * Implements hook_block_view().
 *
 * Defines Ubivox Subscribtion form block.
 */
function ubivox_block_view($delta = '') {
  $block = array();
  $active_list_ids = variable_get('ubivox_active_maillists');
  $all_lists = _ubivox_get_maillists();
  $options = array();
  foreach ($active_list_ids as $active_list_id) {
    if (isset($all_lists[$active_list_id])) {
      $options[$active_list_id] = $all_lists[$active_list_id];
    }
  }
  $form = drupal_get_form('ubivox_subscribe_form', array('maillists' => $options));
  switch ($delta) {
    case 'ubivox_subscribe_form':
      $block['subject'] = t('Subscribe');
      $block['content'] = $form;
      break;

  }
  return $block;
}
